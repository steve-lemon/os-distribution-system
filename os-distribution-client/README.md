# OS Distribution System

This project is to improve and open source code for OS Distribution System by Wayne Inc. It will be online since June 2019.

This system enables the users to download and update the OS easily.

There are 2 benefits for using this system.

1. The OS provider can manage the distribution of OS in a more profitable way.
2. It is easier for the users to download & install OS on the USB. 
